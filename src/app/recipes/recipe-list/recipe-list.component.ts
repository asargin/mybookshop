import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe('A test recipe 1',
    'This is simple test recipe',
    'https://img1.southernliving.timeinc.net/sites/default/files/styles/story_card_two_thirds/public/image/2015/10/main/2311702_qfsau_024.jpg'),
    new Recipe('A test recipe 1',
    'This is simple test recipe',
    'http://assets.blog.foodnetwork.ca/imageserve/wp-content/uploads/2018/01/02143436/vegetarian-cacciatore-recipe/x.jpg')
  ];
  constructor() { }

  ngOnInit() {
  }

}
